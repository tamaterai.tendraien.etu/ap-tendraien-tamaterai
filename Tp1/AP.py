#TENDRAIEN tamaterai
s="Je n'ai jamais joué de flûte."
l = s.split()

def sort(s: str) -> str:
    """
    Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.

    Précondition: aucune

    Exemples:

    $$$ sort('timoleon')
    'eilmnoot'
    """
    l=list(s)
    l.sort()
    chaine=""
    for i in range (len(l)):
        chaine+=l[i]    
    return chaine
###--
def occurrences_v2(chaine: str) -> dict[str, int]:
    """
    renvoie le dictionnaire des associations <lettre, nombre d'occurrences de lettre dans la chaîne>

    Précondition: aucune
    """
    res = {}
    for lettre in chaine:
        res[lettre] = res.get(lettre, 0) + 1
    return res

def verifie_occurence(chaine1:str,chaine2:str)->bool:
    """compte le nombre de lettre de chacune des chaine

    Précondition : len(chaine1)==len(chaine2),ne doivent pas contenir de majuscules/minuscules ni de lettres accenntuées
    Exemple(s) :
    $$$
    """
    d1=occurrences_v2(chaine1)
    d2=occurrences_v2(chaine2)
    return d1==d2
###--

def minuscule(chaine:str)->str:
    """renvoie chaine en lettre minuscule et sans majuscule

    Précondition : 
    Exemple(s) :
    $$$ minuscule('OrangE')
    'orange'
    """
    chai=''
    for elt in chaine:
        chai+=elt.lower()
    return chai

EQUIV_NON_ACCENTUE={'à':'a','â':'a','ä':'a','é':'e','è':'e','ê':'e','ë':'e','ï':'i','î':'i','ô':'o','ö':'o','ù':'u','û':'u','ü':'u','ÿ':'y','ç':'c'}
def bas_casse_sans_accent(s:str)->str:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    """
    chaine=minuscule(s)
    chai=''
    for elt in chaine:
        if elt in EQUIV_NON_ACCENTUE:
            chai+=EQUIV_NON_ACCENTUE[elt]
        else:
            chai+=elt
    return chai

    
def sont_anagrammes(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    True
    """
    chaine1=bas_casse_sans_accent(s1)
    chaine2=bas_casse_sans_accent(s2)
    return verifie_occurence(chaine1,chaine2)

from lexique import LEXIQUE

def recherche_anagramme(mot:str)->list[str]:
    """ Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.
    """
    liste=[]
    for elt in LEXIQUE:
        if sont_anagrammes(mot,elt):
            liste.append(elt)
    return liste

# il n'est pas raisonnable de prendre les mots du LEXIQUE
# pour clé car il y en a bcp trop de plus ils aurait comme valeurs qu'un seul
# anagramme.

def cle(mot:str)->str:
    """renvoie la version triée des lettres du mot après transformation du mot en bas de casse sans accent.

    Précondition :
    Exemple(s) :
    $$$ cle('Orangé')
    'aegnor'
    """
    return sort(bas_casse_sans_accent(mot))


ANAGRAMMES={}
for elt in LEXIQUE:
    k=cle(elt)
    if k in ANAGRAMMES:
        ANAGRAMMES[k].append(elt)
    else:
        ANAGRAMMES[k]=[elt]
        
def recherche_anagramme2(mot:str)->list[str]:
    """Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.
    """
    


    
            
        