#TENDRAIEN - Tamaterai
#07/02/2024
#TP: gestion d'une promotion

from etudiant import Etudiant
from date import Date

def pour_tous(seq_bool: list[bool]) -> bool: ### Cette fonction realise ce que realise la fonction all.
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:
    $$$ pour_tous([True, True, True])
    True
    $$$ pour_tous([True, False, True])
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]==True:
        i=i+1
    return i==len(seq_bool)

def il_existe(seq_bool: list[bool]) -> bool: ### Cette fonction realise ce que realise la fonction any. 
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe([False, False, True])
    True
    $$$ il_existe([False, False])
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]==False:
        i=i+1      
    if i<len(seq_bool):
        res=True       
    else:
        res=False       
    return res
####

def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

L_ETUDIANT=charge_fichier_etudiants('etudiants.csv')
COURTE_LISTE=L_ETUDIANT[:10] 

def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    
    return all(isinstance(elt,Etudiant) for elt in x)

NBRE_ETUDIANTS=len(L_ETUDIANT)
NIP=42313500
nb=NIP%NBRE_ETUDIANTS
ETUDIANT_moinsde20=[L_ETUDIANT[i] for i in range(NBRE_ETUDIANTS) if L_ETUDIANT[i].naissance>Date(2, 2, 2004)]
# Q4: L_ETUDIANTS contient 603 étudiants.
# Q5: L'l'étudiant se trouvant à l'indice égal à mon numéro d'étudiants modulo le nombre d'étudiants est André MARCHAND.
def ensemble_des_formations(liste: list[Etudiant]) -> set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """    
    return {L_ETUDIANT[i].formation for i in range(len(liste))}

def occurence_prenom(liste:list)->dict:
    """Renvoie un dictionnaire avec pour clé des prénoms et pour valeurs
    le nombre d'étudiants de la liste ayant le même prénom.

    Précondition : 
    Exemple(s) :
    $$$ occurence_prenom(COURTE_LISTE)
    {'Eugène': 1, 'Josette': 1, 'Benoît': 1, 'David': 2, 'Andrée': 1, 'Cécile': 1, 'Christiane': 1, 'Paul': 1, 'Anne': 1}
    """
    
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res
# Q8:1) Il y a 3 étudiants ayant pour prénom Alexandre et 2 ayant pour prénoms Camille.
#    2) Cette fonction parcours succéssivement chaques élément de la liste paramétré, il y a un seul parcours. 
# Q9: il y a 198 prénoms différents dans L_ETUDIANT

def occurences_nip(liste:list)->bool:
    """Renvoie True ssi les nip sont distincts
    Précondition : aucune
    Exemple(s) :
    $$$ 

    """
    res = {}
    for etud in liste:
        nip = etud.nip
        if nip in res:
            res[nip] = res[nip] + 1
        else:
            res[nip] = 1
    return len(res)==len(liste)
