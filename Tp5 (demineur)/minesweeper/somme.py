import sys

def main():
    a = int(sys.argv[1])
    b = int(sys.argv[2])
    print(f"la somme de {a} et {b} est {a+b}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage : python3 somme.py <a> <b>")
    else:
        main()

