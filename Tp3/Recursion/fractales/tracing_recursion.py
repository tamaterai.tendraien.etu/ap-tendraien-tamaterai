# Tendraien-Tamaterai
# 31/01/2024
# TP3:récursion

from ap_decorators import *

@trace
def fact(n: int) -> int:
    """
    Renvoie n!
    Condition d'utilisation : n >= 0
    """
    if n == 0:
       res = 1
    else:
       res = n * fact(n - 1)
    return res

def somme_rec(a:int,b:int)->int:
    """renvoie la samme de a et b.
    Précondition : a>=0
    Exemple(s) :
    $$$ somme_rec(1,1)
    2
    $$$ somme_rec(0,0)
    0
    $$$ somme_rec(0,4)
    4
    $$$ somme_rec(4,0)
    4
    """
    if a>0:
        res=somme_rec(a-1,b+1)
    elif a==0:
        res=b
    return res
@trace       
def binomial(n:int,p:int):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : n>=p>=0
    Exemple(s) :
    $$$ binomial(4,2)
    6
    $$$ binomial(1,1)
    1
    $$$ binomial(0,0)
    1
    """
    if p==0 or n==p:
        res= 1
    else:
        res=binomial(n-1,p)+binomial(n-1,p-1)
    return res
@trace
def is_palindromic(chaine:str)->bool:
    """renvoie true ssi chaine est un palindrome
    Précondition : i==0
    Exemple(s) :
    $$$ is_palindromic('radar')
    True
    """
    if len(chaine)==1:
        res=True
    elif chaine[0]==chaine[-1] and is_palindromic(chaine[1:-1]):
        res=is_palindromic(chaine[1:-1])
    else:
        return False
    return res

        
        
    
    

        

    
    