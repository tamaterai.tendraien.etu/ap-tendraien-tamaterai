# Tendraien Tamaterai
# 31/01/2024
# TP3: fibonacci
from ap_decorators import *
@count
@trace
def fibo(n:int)->int:
    """

    Précondition : 
    Exemple(s) :
    $$$ fibo(10)
    55
    """
    if n==0:
        res=0
    elif n==1:
        res=1
    else:
        res=fibo(n-1)+fibo(n-2)
    return res